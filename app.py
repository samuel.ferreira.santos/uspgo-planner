import os
import json
import googlemaps
import requests
import time
import threading
from multiprocessing import Process
from math import sin, cos, sqrt, atan2, radians
from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)
drivers = {}
h_hikers = {}
info_lock = threading.Lock()

campus_1_coords = (-22.006867, -47.897148)
campus_2_coords = (-22.001870, -47.930776)


class InputUser(Resource):
    def post(self):
        global rider
        global driver
        global info_lock
        parser = reqparse.RequestParser()
        parser.add_argument('username', type=str)
        parser.add_argument('originLatitude', type=float)
        parser.add_argument('originLongitude', type=float)
        parser.add_argument('destinationLatitude', type=float)
        parser.add_argument('destinationLongitude', type=float)
        parser.add_argument('status', type=str)
        parser.add_argument('seats_available', type=int)
        args = parser.parse_args()

        with info_lock:
            if args['status'] == 'driver':
                drivers[args['username']] = args
            elif args['status'] == 'h_hiker':
                h_hikers[args['username']] = args

        return {"status": 200, "msg": "user inserted"}


class MatchProcess:
    def __init__(self, info_lock, drivers, h_hikers):
        self.info_lock = info_lock
        self.drivers = drivers
        self. h_hikers = h_hikers

    def generate_match(self):
        temp_h_hikers = None
        temp_drivers = None
        with self.info_lock:
            temp_drivers = self.drivers.copy()
            temp_h_hikers = self.h_hikers.copy()

        for driver in temp_drivers:
            d_going_to = which_campus(driver['destinationLatitude'],
                                      driver['destinationLongitude'])
            match = [driver['username']]
            for index, h_hiker in enumerate(temp_h_hikers):
                h_going_to = which_campus(h_hiker['destinationLatitude'],
                                          h_hiker['destinationLongitude'])
                if d_going_to == h_going_to:
                    if driver['seats_available'] >= 1:
                        driver['seats_available'] -= 1
                        match.append(h_hiker['username'])
                        del temp_h_hikers[index]
                    else:
                        break

            if len(match) > 1:
                self.send_match(users_list=match)

    def drop_matched_users(self, users_list):
        with self.info_lock:
            for user_name in users_list:
                if user_name in drivers:
                    del self.drivers[user_name]
                else:
                    del self.h_hikers[user_name]

    def send_match(self, users_list):
        self.drop_matched_users(users_list)
        data = {
            'match_1': users_list
        }
        requests.post('127.0.0.1/', data=data)

    def find_match(self):
        while True:
            self.generate_match()
            time.sleep(seconds=2)


def distance(lat1, lon1, lat2, lon2):
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c


def which_campus(lat, lon):
    dist_cp1 = distance(lat, lon, *campus_1_coords)
    dist_cp2 = distance(lat, lon, *campus_2_coords)

    return 'campus_1' if dist_cp1 < dist_cp2 else 'campus_2'


api.add_resource(InputUser, '/planner/input')
if __name__ == "__main__":
    match_process = MatchProcess(info_lock, drivers, h_hikers)
    process = Process(target=match_process.find_match)
    process.start()
    app.run(debug=True)
    process.join()


"""
rides = {
    'campus_1': 0,
    'campus_2': 0
}

directions = {
    'campus_1': "USP São Carlos - Campus da Universidade de São Paulo",
    'campus_2': (-22.002331, -47.930211)
}

key = os.environ.get('GMAPS_API_KEY')
if key is None:
    print('Environment variable GMAPS_API_KEY not set')
    quit(1)

gmaps = googlemaps.Client(key=key)
# directions_result = gmaps.directions(campus_1, campus_2, mode='driving')
@app.route('/quero-carona/<campus>')
def registerRide(campus):
    global rides
    rides[campus] = rides[campus] + 1
    print(rides[campus], campus)
    return 'Ok'
@app.route('/quero-dar-carona/<campus>')
def getRiders(campus):
    global rides

    caroneiros = 3 if rides[campus] >= 3 else rides[campus]
    rides[campus] = rides[campus] - caroneiros

    campus_contrario = 'campus_1' if campus == 'campus_1' else 'campus_2'
    return json.dumps({
        'caroneiros': caroneiros,
        'route': gmaps.directions(directions[campus], directions[campus_contrario], mode='driving')
    })
"""
