import socket
import json


data = {
    'Usr': {
        'name': 'Ken Chen',
        'age': '22',
        'loc': '1243, 345'
    }

}


def client_program(data_json):
    host = socket.gethostname()  # as both code is running on same pc
    port = 5001  # socket server port number

    client_socket = socket.socket()  # instantiate
    client_socket.connect((host, port))  # connect to the server

    client_socket.send(data_json.encode())  # send message

    client_socket.close()  # close the connection


if __name__ == '__main__':
    data_json = json.dumps(data)
    client_program(data_json)
